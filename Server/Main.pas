unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Effects, FMX.ScrollBox, FMX.Memo,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdCustomHTTPServer,
  IdHTTPServer, IdContext, FMX.Edit;

type
  TfrmMain = class(TForm)
    swtchServerActivated: TSwitch;
    pnlTop: TPanel;
    mmoOutput: TMemo;
    idServer: TIdHTTPServer;
    lblServerActive: TLabel;
    edtResponse: TEdit;
    pnlBottom: TPanel;
    ShadowEffect2: TShadowEffect;
    Label1: TLabel;
    btnClearConsole: TButton;
    procedure swtchServerActivatedSwitch(Sender: TObject);
    procedure idServerCommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure btnClearConsoleClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

procedure TfrmMain.btnClearConsoleClick(Sender: TObject);
begin
  mmoOutput.Text := '';
end;

procedure TfrmMain.idServerCommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  lParams: TStrings;
  lMessage: string;
  lDateSent: string;
  lServerResponse: string;
begin
  lServerResponse := edtResponse.Text;

  //'Mount' the request parameters
  lParams := ARequestInfo.Params;
  //Retrieve the values from the parameters
  lMessage := lParams.Values['message'];
  lDateSent := lParams.Values['datesent'];

  //Configure the response
  if lServerResponse <> '' then
    AResponseInfo.ContentText := lServerResponse
  else
    AResponseInfo.ContentText := 'Default Response';

  //Update the server console memo
  mmoOutput.Text := mmoOutput.Text + #13#10 + lDateSent + ' - ' + lMessage;
end;

procedure TfrmMain.swtchServerActivatedSwitch(Sender: TObject);
begin
  //Activate/Deactivate the server component & set status label
  idServer.Active := swtchServerActivated.IsChecked;

  if swtchServerActivated.IsChecked then
    lblServerActive.Text := 'Server Active'
  else
    lblServerActive.Text := 'Server Not Active';

end;

end.
