unit USendController;

interface

uses idHttp, Classes, SysUtils;

type
  TSendController = class
  private
    fHTTPClient : TIdHTTP;
  public
    constructor Create;
    destructor Destroy;
    procedure SendCommand(AURL: String; AParams: TStringList; var AResponseVar: String);
  end;

implementation

{ TSendController }

constructor TSendController.Create;
begin
  fHTTPClient := TIdHTTP.Create;
end;

destructor TSendController.Destroy;
begin
  fHTTPClient.Free;
end;

procedure TSendController.SendCommand(AURL: String; AParams: TStringList; var AResponseVar: String);
begin
  if (AURL[1] <> 'h' ) and (AURL[2] <> 't') then
    AURL := 'http://' + AURL;

  //Simply initiate a HTTP POST and assign the response to the response variable
  try
    AResponseVar := fHTTPClient.Post(AURL, AParams);
  except
    on E: Exception do begin
      AResponseVar := E.Message;
    end;
  end;
end;

end.
