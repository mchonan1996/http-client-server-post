unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,

  USendController;

type
  TfrmMain = class(TForm)
    edtURL: TEdit;
    edtMessageBox: TEdit;
    btnSend: TButton;
    mmoOutput: TMemo;
    btnClearConsole: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnClearConsoleClick(Sender: TObject);
  private
    { Private declarations }
    fSendController: TSendController;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

procedure TfrmMain.btnClearConsoleClick(Sender: TObject);
begin
  mmoOutput.Text := '';
end;

procedure TfrmMain.btnSendClick(Sender: TObject);
var
  lURL: string;
  lMessage: string;
  lParams: TStringList;
  lSentDate: string;
  lResponse: string;
begin
  //User clicks send
  lURL := edtURL.Text;
  lResponse := '';
  lSentDate := FormatDateTime('dd/mm/yy', now);
  lMessage := edtMessageBox.Text;
  edtMessageBox.Text := '';

  //Set up the parameters for the HTTP command
  lParams := TStringList.Create;
  try
    lParams.Add('message=' + lMessage); //You can change message to be whatever parameter name you want
    lParams.Add('datesent=' + lSentDate); //Add as many parameters as you want too

    //Send the command using or send controller (adding a response variable to store the server response)
    fSendController.SendCommand(lURL, lParams, lResponse);

    //Update the console memo
    if lResponse <> '' then
      mmoOutput.Text := mmoOutput.Text + #13#10 + 'Server: ' + lResponse
    else
      mmoOutput.Text := mmoOutput.Text + #13#10 + 'Server did not respond';
  finally
    lParams.Free;
  end;

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  fSendController := TSendController.Create;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  fSendController.Free;
end;

end.
